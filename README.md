# Help Center Code
Kdan Dottedsign Helper center Page.  
[Demo URL](https://support.kdanmobile.com/hc/en-us)

## HTML Template
[handlebarsjs](https://handlebarsjs.com/guide/)

## Project Design Patterns
1.基本上 CSS、HTML、JS 都盡量寫在同一隻檔案內，CSS 如果沒找到需要到全域去找（ style.css ）．  
2.因為 CSS 全域會牽扯到很多檔案，所以有些 CSS 不會寫在特檔案底下，會複製一份並在前面加上該部分 class 名稱．
```scss
// 全域、保留
.page-header {
  flex-shrink: 0;
}
// 在 section 底下加入新的
.section-container .page-header {
  margin-bottom: 10px;
}
```

## Folder Structure
```
.
├── src
│   ├── article
│   ├── category
│   ├── footer
│   ├── header
│   ├── homepage
│   ├── section
│   └── origin ..原始檔案
├── script.js ..全域JS
├── style.css ..全域CSS
└── README.md
```

## JS Library
使用 vanilla js 去控制 DOM 或其他特效．  
  
1.  控制麵包屑最後一個顏色為特定顏色．（script.js）
2.  Menu icon 切換．
3.  Header bar 滾動切換 `boxShadow`．
4.  Footer DropDownMenu icon change.
5.  Help center button color.（script.js）
6.  Article page add article name after breadcrumbs.
7.  Remove categories breadcrumbs.
8.  Home page icon render.
9.  連結語系轉換成與 zendesk 一樣的

## Link Locales
透過 `filterLinkLocales` 這個 function 去更換連結裡面有 `zh-tw` 的字為現在 zendesk 語系，如此一來跳頁便會跟現在語系相同．  
日後加入或修改連結須以 `zh-tw` 為主．
```html
## 現在 zendesk 語系是 zh-cn
<a
  href="https://www.dottedsign.com/zh-tw/security"
></a>

## 轉換
<a
  href="https://www.dottedsign.com/zh-cn/security"
></a>
```

## Zendesk API
1.You can use [Zendesk API](https://api.zopim.com/files/meshim/widget/controllers/LiveChatAPI-js.html) to do something what you want to change in zendesk website.  
2.Using the Zendesk [HelpCenter Javascript Object](https://www.screensteps.com/articles/customizing-your-help-center-with-javascript) in your Customizations  
3.[Help Center API](https://developer.zendesk.com/rest_api/docs/help_center/introduction).  
4.This is about how to login zendesk's article, [Remove "Sign In" from Help Center](https://support.zendesk.com/hc/en-us/community/posts/203412826-Remove-Sign-In-from-Help-Center?page=3).


## Responsive Web Design
#### Breakpoint
+ 768 px
+ 1160 px
+ 1024 px
#### Use media
```css
@media (min-width: 768px) { ... }
```
